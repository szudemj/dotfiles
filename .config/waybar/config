{
    // Global Settings
    "layer": "top",
    "position": "top",
    "height": 30,
    "modules-left": [
        "sway/workspaces",
        "sway/window"
    ],
    "modules-center": [
        "network#bandwidth"
    ],
    "modules-right": [
        "idle_inhibitor",
        "custom/vpn",
        "network",
        "memory",
        "cpu",
        "pulseaudio",
        "battery",
        "clock"
    ],
    // Module Settings
    "idle_inhibitor": {
        "format": "{icon} ",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "battery": {
        "states": {
            "warning": 30,
            "critical": 15
        },
        "format": "{icon} {capacity}%",
        "format-charging": " {capacity}%",
        "format-plugged": " {capacity}%",
        "format-icons": [
            "",
            "",
            "",
            "",
            ""
        ]
    },
    "clock": {
        "interval": 10,
        "format": "{:%e %b %Y  %H:%M}",
        "tooltip-format": "<tt><small>{calendar}</small></tt>",
        "calendar": {
            "mode": "month",
            "mode-mon-col": 3,
            "weeks-pos": "left",
            "on-scroll": 1,
            "on-click-right": "mode",
            "format": {
                "months": "<span color='#ffead3'><b>{}</b></span>",
                "days": "<span color='#ecc6d9'><b>{}</b></span>",
                "weeks": "<span color='#c300ff'><b>W{}</b></span>",
                "weekdays": "<span color='#03d3fc'><b>{}</b></span>",
                "today": "<span color='#ff9900'><b><u>{}</u></b></span>"
            }
        },
        "actions": {
            "on-click-right": "mode",
            "on-click-forward": "tz_up",
            "on-click-backward": "tz_down",
            "on-scroll-up": "shift_down",
            "on-scroll-down": "shift_up"
        }
    },
    "cpu": {
        "interval": 5,
        "format": "󰓅 {usage}% ({load})",
        "states": {
            "warning": 70,
            "critical": 90
        },
        "on-click": "alacritty -e 'htop'"
    },
    "memory": {
        "interval": 5,
        "format": "󰍛 {}%",
        "on-click": "alacritty -e 'htop'",
        "states": {
            "warning": 70,
            "critical": 90
        }
    },
    "network": {
        "interval": 5,
        "format-wifi": "  {signalStrength}%",
        "format-ethernet": " {ifname}: {ipaddr}/{cidr}",
        "format-disconnected": "⚠ Disconnected",
        "tooltip-format": "{ipaddr} | {essid} {frequency}",
        "on-click": "alacritty -e 'nmtui'"
    },
    "network#bandwidth": {
        "interval": 3,
        "format": "󰁅 {bandwidthDownBytes} 󰁝 {bandwidthUpBytes}",
        "on-click": "alacritty -e 'btm'"
    },
    "custom/vpn": {
        "exec": "/home/knowone/.config/waybar/scripts/vpn.sh",
        "interval": 5,
        "return-type": "json",
        "on-click": "mullvad-vpn"
    },
    "sway/window": {
        "format": "{}",
        "max-length": 120
    },
    "sway/workspaces": {
        "disable-scroll": true,
        "disable-markup": false,
        "all-outputs": true,
        "format": "{icon}",
        "format-icons": {
            "10": ""
        }
    },
    "pulseaudio": {
        "scroll-step": 1,
        "format": "{icon} {volume}%",
        "format-bluetooth": " {icon} {volume}% {format_source}",
        "format-bluetooth-muted": " 󰓄 {format_source}",
        "format-muted": "󰓄",
        "format-source": " {volume}%",
        "format-source-muted": "",
        "format-icons": {
            "headphone": "󰓃",
            "hands-free": "󰓃",
            "headset": "󰓃",
            "phone": "",
            "portable": "",
            "car": "",
            "default": [
                "󰓃"
            ]
        },
        "on-click": "pavucontrol",
        "on-scroll-up": "wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%+ --limit 1",
        "on-scroll-down": "wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%-"
    },
    "backlight#icon": {
        "format": "{icon}",
        "format-icons": [
            ""
        ],
        "on-scroll-down": "brightnessctl -c backlight set 1%-",
        "on-scroll-up": "brightnessctl -c backlight set +1%"
    },
    "backlight#value": {
        "format": "{percent}%",
        "on-scroll-down": "brightnessctl -c backlight set 1%-",
        "on-scroll-up": "brightnessctl -c backlight set +1%"
    }
}